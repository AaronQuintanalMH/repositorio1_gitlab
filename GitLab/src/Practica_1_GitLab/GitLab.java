package Practica_1_GitLab;

public class GitLab {
    public static void main(String[] args){
        /*
        *   Entornos de Desarrollo
        *   Práctica 1 GitLab
        *   09/11/2021
        *   Aarón Quintanal Martin
        */
        
        System.out.println("Mi primer commit desde NetBeans");
        System.out.println("Commit hecho desde GitLab");
        
        // commit para v4
        System.out.println("System v.4");
    }
}
